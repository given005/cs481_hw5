﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace cs481_hw5_1
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        
        public MainPage()
        {
            InitializeComponent();
            //sets map to my home town
            var intial = MapSpan.FromCenterAndRadius(new Position(33.769324, -116.957634), Distance.FromMiles(1));
            StreetView.MoveToRegion(intial);

            var pos1 = new Position(33.739796, -116.936326);
            var pos2 = new Position(33.729506, -116.933611);
            var pos3 = new Position(33.719972, -116.949818);
            var pos4 = new Position(33.806836, -116.969324);
            //creates pins to mark locations
            var pin = new Pin
            {
                Type = PinType.Place,
                Position = pos1,
                Label = "Middle school I attended"


            };
            var pin2 = new Pin
            {
                Type = PinType.Place,
                Position = pos2,
                Label = "High school I attended"


            };
            var pin3 = new Pin
            {
                Type = PinType.Place,
                Position = pos3,
                Label = "Where my Graduation took place"


            };
            var pin4 = new Pin
            {
                Type = PinType.Place,
                Position = pos4,
                Label = "Community college I attended"


            };
            StreetView.Pins.Add(pin);
            StreetView.Pins.Add(pin2);
            StreetView.Pins.Add(pin3);
            StreetView.Pins.Add(pin4);
            
        }
        


        
       
        //picker to hold location of pins
        private void picker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;

            int selectedIndex = picker.SelectedIndex;
            if(selectedIndex == 0)
            {
                var pos1 = MapSpan.FromCenterAndRadius(new Position(33.739796, -116.936326), Distance.FromMiles(1));
                StreetView.MoveToRegion(pos1);
            
            }
            else if(selectedIndex == 1)
            {
                var pos2 = MapSpan.FromCenterAndRadius(new Position(33.729506, -116.933611), Distance.FromMiles(1));
                StreetView.MoveToRegion(pos2);
            }
            else if(selectedIndex == 2)
            {
                var pos3 = MapSpan.FromCenterAndRadius(new Position(33.719972, -116.949818), Distance.FromMiles(1));
                StreetView.MoveToRegion(pos3);
            }
            else if(selectedIndex == 3)
            {
                var pos4 = MapSpan.FromCenterAndRadius(new Position(33.806836, -116.969324), Distance.FromMiles(1));
                StreetView.MoveToRegion(pos4);
            }
        }
        //switches from street view to satellite view
        async void OnNextPageButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Satellite());
        }
    }
}
