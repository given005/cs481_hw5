﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace cs481_hw5_1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Satellite : ContentPage
    {
        public Satellite()
        {
            InitializeComponent();
            var intial = MapSpan.FromCenterAndRadius(new Position(33.769324, -116.957634), Distance.FromMiles(1));
            SatView.MoveToRegion(intial);

            var pos1 = new Position(33.739796, -116.936326);
            var pos2 = new Position(33.729506, -116.933611);
            var pos3 = new Position(33.719972, -116.949818);
            var pos4 = new Position(33.806836, -116.969324);
            var pin = new Pin
            {
                Type = PinType.Place,
                Position = pos1,
                Label = "Middle school I attended"
                

            };
            var pin2 = new Pin
            {
                Type = PinType.Place,
                Position = pos2,
                Label = "High school I attended"
               

            };
            var pin3 = new Pin
            {
                Type = PinType.Place,
                Position = pos3,
                Label = "Where my Graduation took place"
               

            };
            var pin4 = new Pin
            {
                Type = PinType.Place,
                Position = pos4,
                Label = "Community college I attended"
                

            };
            SatView.Pins.Add(pin);
            SatView.Pins.Add(pin2);
            SatView.Pins.Add(pin3);
            SatView.Pins.Add(pin4);

        }






        private void picker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;

            int selectedIndex = picker.SelectedIndex;
            if (selectedIndex == 0)
            {
                var pos1 = MapSpan.FromCenterAndRadius(new Position(33.739796, -116.936326), Distance.FromMiles(1));
                SatView.MoveToRegion(pos1);

            }
            else if (selectedIndex == 1)
            {
                var pos2 = MapSpan.FromCenterAndRadius(new Position(33.729506, -116.933611), Distance.FromMiles(1));
                SatView.MoveToRegion(pos2);
            }
            else if (selectedIndex == 2)
            {
                var pos3 = MapSpan.FromCenterAndRadius(new Position(33.719972, -116.949818), Distance.FromMiles(1));
                SatView.MoveToRegion(pos3);
            }
            else if (selectedIndex == 3)
            {
                var pos4 = MapSpan.FromCenterAndRadius(new Position(33.806836, -116.969324), Distance.FromMiles(1));
                SatView.MoveToRegion(pos4);
            }
        }


         async void OnPreviousPageButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
        }
    }

